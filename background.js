function simplifyPage() {
  var x = document.getElementsByClassName("slider-item");
  var values = [];
  while (values.length < x.length / 2){
      var y = Math.floor(Math.random() * x.length);
      if (!values.includes(y)){
          values.push(y);
      }
  }

  for (var z = 0; z < x.length; z++){
     if (values.includes(z)){
         x[z].remove();
     }
  }

  x = document.getElementsByClassName("slider-item");
 for (var z = 0; z < x.length; z++){
    var w = x[z].getElementsByClassName("loadingTitle");
    if (w.length > 0)
	w[0].remove();
  }
}

chrome.action.onClicked.addListener((tab) => {
  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    function: simplifyPage
  });
});